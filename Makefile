# SPDX-License-Identifier: AGPL-3.0-or-later

PREFIX ?= /usr/local

default: doc

doc:
	makeinfo --number-sections       eltr.texi
	texi2pdf eltr.texi

test:
	@echo "ELTR tests will start, using the ERT test framework."
	@echo "The output from ERT will be sent to stdout, where you can see,"
	@echo "and the output from ELTR, including its error messages, will"
	@echo "be redirected to the file test-stdout.log.\n"
	@emacs -batch -l ert -l ./eltr-test.el -f ert-run-tests-batch-and-exit 1> test-stdout.log

clean:
	rm -f test-stdout.log \
		eltr.log \
		eltr.aux \
		eltr.cp \
		eltr.cps \
		eltr.toc

.PHONY: install
install:
	install eltr      $(DESTDIR)$(PREFIX)/bin
	install eltr.el   $(DESTDIR)$(PREFIX)/share/emacs/site-lisp/
	install eltr.info $(DESTDIR)$(PREFIX)/share/info/
	install-info      --info-dir=$(DESTDIR)$(PREFIX)/share/info/ $(DESTDIR)$(PREFIX)/share/info/eltr.info

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/eltr
	rm -f $(DESTDIR)$(PREFIX)/share/emacs/site-lisp/eltr.el
	rm -f $(DESTDIR)$(PREFIX)/share/info/eltr.info
