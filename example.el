;; SPDX-License-Identifier: AGPL-3.0-or-later

;; this file contains the example expressions for
;; the screnshot

(setq buf (get-buffer-create "new-buffer"))
(with-current-buffer buf (insert "Hello, World!"))
(buffer-string)
(switch-to-buffer buf)
(buffer-string)
(message "Where is the minibuffer? :)")
(/ (+ (sqrt 5) 1) 2)
'a-symbol
unquoted-symbol
,,e7                ;; seventh input expression
,,v7                ;; seventh returned value
(setq x (read))     ;; IELM doesn't handle this
(concat "a " "string")
x
70
(setq eltr-show-max-binary-digits 4)
70
(setq eltr-result-color 'yellow)
(setq eltr-result-suffix-emphasis 'underline)
