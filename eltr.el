;;; eltr.el --- Emacs Lisp Terminal REPL -*- lexical-binding: t; -*-

;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Copyright (C) 2020 Jerônimo C. Pellegrini

;; Author: Jeronimo Pellegrini <j_p@aleph0.info>
;; Maintainer: Jeronimo Pellegrini <j_p@aleph0.info>

;; Keywords: terminal repl lisp

;; ELTR is free software: you can redistribute it and/or modify
;; it under the terms of the Affero GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; ELTR is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with ELTR.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

;;;
;;; ELTR VERSION STRING
;;;

(defvar eltr-version "0.0.4")

;;;
;;; CUSTOMIZATION
;;;

(defgroup eltr nil
  "Emacs Lisp Terminal REPL."
  :link '(url-link "https://gitlab.com/jpellegrini/eltr")
  :group 'languages)

(defcustom eltr-show-banner t
  "If true, ELTR will greet the user with a banner.")

(defcustom eltr-banner
  (concat "ELTR version $(version).\n"
          "Emacs version $(emacs-version), "
          "built $(emacs-build-time) on $(emacs-build-system).\n"
          "It will be a nice day. I can feel it!\n")
  "Banner printed when ELTR starts.")

;; We DO use dynamic scope to set this to "" when continuing to
;; read a string in next line.
(defcustom eltr-prompt-prefix "$(nexpr)> "
  "Prompt to be displayed when asking for user input."
  :group 'eltr
  :type 'string)

(defcustom eltr-result-prefix "$(nexpr)=> "
  "String to be displayed before showing evaluation result."
  :group 'eltr
  :type 'string)

(defcustom eltr-result-suffix "\n                       ( $(tvalue) )"
  "String to be displayed before showing evaluation result."
  :group 'eltr
  :type 'string)

(defcustom eltr-echo-input-prefix ";;"
  "Default prefix to input echo."
  :group 'eltr
  :type 'string)

(defcustom eltr-echo-input t
  "Flag that tells wether the input should be echoed before eval."
  :group 'eltr
  :type 'boolean)


;; colors

(defcustom eltr-colorize t
  "Should ELTR use color when printing?"
  :group 'eltr
  :type 'boolean)

(defcustom eltr-prompt-color 'magenta
  "Color for the ELTR prompt."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-prompt-emphasis 'bold
  "Emphasis (normal, bold, underline) for the ELTR prompt."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-result-prefix-color 'white
  "Color for the ELTR prefix."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-result-prefix-emphasis 'normal
  "Emphasis (normal, bold, underline) for the ELTR prefix."
  :group 'eltr
  :type 'symbol)


(defcustom eltr-result-color 'white
  "Color for the displayed result."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-result-emphasis 'normal
  "Emphasis (normal, bold, underline) for the displayed result."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-result-suffix-color 'cyan
  "Color for the result suffix."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-result-suffix-emphasis 'normal
  "Emphasis (normal, bold, underline) for the result suffix."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-echo-prefix-color 'white
  "Color for the input echo prefix."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-echo-prefix-emphasis 'normal
  "Emphasis (normal, bold, underline) for the input echo prefix."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-echo-color 'white
  "Color for the input echo."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-echo-emphasis 'normal
  "Emphasis (normal, bold, underline) for the input echo."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-error-color 'red
  "Color for error messages."
  :group 'eltr
  :type 'symbol)

(defcustom eltr-error-emphasis 'bold
  "Emphasis (normal, bold, underline) for error messages."
  :group 'eltr
  :type 'symbol)


;; integers

(defcustom eltr-show-int-as-char t
  "Also show integers as characters on output?"
  :group 'eltr
  :type 'boolean)

(defcustom eltr-show-int-as-hex t
  "Also show integers in hexadecimal representation on output?"
  :group 'eltr
  :type 'boolean)

(defcustom eltr-show-int-as-bin t
  "Also show integers in binary representation on output?"
  :group 'eltr
  :type 'boolean)


(defcustom eltr-show-int-as-octal t
  "Also show integers in octal representation on output?"
  :group 'eltr
  :type 'boolean)

(defcustom eltr-show-max-dec-digits 90
  "Maximum quantity of digits used in decimal representations of integers on output."
  :group 'eltr
  :type 'boolean)

(defcustom eltr-show-max-hex-digits 30
  "Maximum quantity of digits used in hex representations of integers on output."
  :group 'eltr
  :type 'boolean)

(defcustom eltr-show-max-binary-digits 30
  "Maximum quantity of digits used in binary representations of integers on output."
  :group 'eltr
  :type 'boolean)

(defcustom eltr-show-max-octal-digits 30
  "Maximum quantity of digits used in octal representations of integers on output."
  :group 'eltr
  :type 'boolean)


;; history

(defcustom eltr-history-size 100
  "Size of history kept."
  :group 'eltr
  :type 'integer)


;;;
;;; VARS
;;;

(defvar eltr--next-expr 1
  "Index of the next expression to be sent to eval.")

(defvar eltr--open-parenses-count 0
  "Number of parentheses left open in a line.
we use this to tell wether we keep reading lines or finish
reading and send the form to eval -- this allows the user to
write multi-line expressions.")

(defvar eltr--read-state 'out-of-string
  "State of the input reader.
eltr--read-state can be:
* 'out-of-string -- reading a Lisp expression, not inside a
  string and not escaping a char
* 'out-of-string-escaping -- reading a Lisp expression, not
  inside a string but escaping a char
* 'in-string -- reading a string
* 'in-string-escaping -- readig a string, and escaping a char")

(defvar eltr--history nil
  "A history of the REPL, represented as a list.")

;;;
;;; CODE BEGINS HERE
;;;


;;;
;;; ERRORS
;;;

;; The cl-lib library has an assert macro, but since this is the only thing
;; we'd need from cl-lib, we can instead define it here, with a different name
;; (eltr-assert).
;;
;; eltr-assert should be used to detect errors in ELTR itself, not user
;; errors.
(defmacro eltr-assert (test-form)
  "A simple assert macro.  Will eval TEST-FORM and signal an error if it's NIL."
  `(unless ,test-form
     (error (eltr-colorize-string  (format "ELTR: assertion failed -- %s" ',test-form)
                                  eltr-error-emphasis
                                  eltr-error-color))))

(defun eltr-print-error (str)
  "Colorizes and prints the error string STR."
  (eltr-assert (stringp str))
  (princ (eltr-colorize-string (concat "[E]: " str)
                               eltr-error-emphasis
                               eltr-error-color)))


;;;

;;; Commentary:
;; 

;;; HISTORY
;;;

;; The following are accessors for history items.
;; FIXME: use something nicer than cadr, caddr and cadddr!
;;
;; For now, history items are stored as
;;
;; (INDEX EXPRESSION FAILURE VALUE)  => with FAILURE = NIL
;; or
;; (INDEX EXPRESSION FAILURE)        => with FAILURE = T
;;
(defun eltr-history-item-expression (item)
  "Return the expression of a history ITEM."
  (cadr item))

(defun eltr-history-item-value (item)
  "Return the value of a history ITEM."
  (cadddr item))

(defun eltr-history-item-failed (item)
  "Return T or NIL for a history ITEM, depending wether it failed.
Given a history item, this function's return value tells wether
the expression failed to evaluate (T) or evaluated
properly (NIL)."
  (caddr item))




(defun eltr-history-ref (n)
  "Return a LIST: either (N expression #f value) or (N expression #t) or NIL."
  (assoc n eltr--history))

(defun eltr-push-history (&rest args)
  "Includes a new element in the REPL history.
ARGS should be (expr NIL value) or \(expr T)."
  (eltr-assert (member (length args) '(2 3)))
  (eltr-assert (booleanp (cadr args)))
  (let ((new-hist (cons eltr--next-expr args)))
    (push new-hist eltr--history)
    (while (> (length eltr--history) eltr-history-size)
      (setq eltr--history (butlast eltr--history)))))


;;;
;;; COLOR
;;;

;; The color-code here is taken from STklos Scheme (which is
;; licenced under "GPLv2 or later").
(defun eltr-color-code (c)
  "Return the ANSI color code C for a color or emphasis decoration."
  (eltr-assert (symbolp c))
  (let ((alist '((normal      . "0")
                 (bold        . "1")  (no-bold        . "21")
                 (italic      . "2")  (no-italic      . "22")
                 (underline   . "4")  (no-underline   . "24")
                 (blink       . "5")  (no-blink       . "25")
                 (reverse     . "7")  (no-reverse     . "27")
                 (black       . "30") (bg-black       . "40")
                 (red         . "31") (bg-red         . "41")
                 (green       . "32") (bg-green       . "42")
                 (yellow      . "33") (bg-yellow      . "43")
                 (blue        . "34") (bg-blue        . "44")
                 (magenta     . "35") (bg-magenta     . "45")
                 (cyan        . "36") (bg-cyan        . "46")
                 (white       . "37") (bg-white       . "47"))))
    (let ((v (assoc c alist)))
      (if v (cdr v) ""))))

(defun eltr-colorize-string (str emphasis color)
  "Colorizes and puts emphasis in a string.
Return STR, prefixed and suffixed so it will be printed in COLOR,
and with EMPHASIS on displays that suport ANSI color escape
codes.  The 'normal/white' code is appended, so text *after* this
string will be in normal white color, unless more escape codes
are inserted."
  (eltr-assert (stringp str))
  (eltr-assert (symbolp emphasis))
  (eltr-assert (symbolp color))
  (if eltr-colorize
      (let ((trimmed (string-trim-left str)))
        (let ((diff (- (length str) (length trimmed))))
          (let ((prefix (substring str 0 diff)))
            (format "%s\e[%s;%sm%s\e[%s;%sm"
                    prefix     ;; empty space, don't colorize
                    (eltr-color-code emphasis)
                    (eltr-color-code color)
                    trimmed
                    (eltr-color-code 'normal)
                    (eltr-color-code 'white)))))
    str))


;;;
;;; STARTUP AND CLEANUP
;;;

(defun eltr-hello ()
  "Prints a banner before the REPL starts."
  (let ((subs (list (cons "$(version)" eltr-version)
                    (cons "$(emacs-version)" emacs-version)
                    (cons "$(emacs-bzr-version)" emacs-bzr-version)
                    (cons "$(emacs-build-time)"  (format-time-string "%Y-%m-%d %H:%M:%S" emacs-build-time))
                    (cons "$(emacs-build-system)" emacs-build-system)))
        (str eltr-banner))
    (dolist (sub subs)
      (let ((key (car sub))
            (text (cdr sub)))
        (setq str (replace-regexp-in-string key text str))))
    (princ str))
  (unless noninteractive ;; that is, within Emacs
    (princ "                 (press a key)")
    (read-char)))

(defun eltr-cleanup ()
  "Cleans up before exiting ELTR.
This makes sense when the user calls ELTR ifrom inside Emacs.
After using ,q and exiting ELTR, the user may call \\[eltr]
again, and will be confused when the next inpput label is not 1."
  (setq eltr--next-expr 1
        eltr--history   nil))


;;;
;;; TEXT-HANDLING FUNCTIONS
;;;

(defun eltr-next-nonblank (str start)
  "Return the POSITION of the first non-blank character of STR.
The search begins at position 'START'.  If the string has only
blank characters, the result is nil."
  (string-match "[^[:space:]]" str start))


(defun eltr-substitute-placehloders (str)
  "Substitute $(ninputs) and $(nevals).
This function will substitute the number of inputs and evals in
STR for $(ninputs) and $(nevals) respectively.
FIXME: this is a quick hack."
  (eltr-assert (stringp str))
  (let ((subs (list (cons "$(tvalue)"   (symbol-name
                                         (type-of
                                          (eltr-history-item-value
                                           (eltr-history-ref eltr--next-expr)))))
                    (cons "$(nexpr)"    (number-to-string eltr--next-expr)))))
    (dolist (sub subs)
      (let ((key (car sub))
            (text (cdr sub)))
        (setq str (replace-regexp-in-string key text str)))))
  str)

(defun eltr-count-parenses (str)
  "Counts the number of open parenses in STR.
Ignores anything after a comment of an extra closed paren."
  (eltr-assert (stringp str))
  (catch 'unbalanced
    (catch 'comment
      (dotimes (i (length str))
        (pcase eltr--read-state
          ('out-of-string
           (pcase (aref str i)
             (?\; (throw 'comment (substring str 0 (- i 1))))
             (?\" (setq eltr--read-state 'in-string))
             (?\( (setq eltr--open-parenses-count (+ eltr--open-parenses-count 1)))
             (?\) (setq eltr--open-parenses-count (- eltr--open-parenses-count 1)))
             (?\\ (setq eltr--read-state 'out-of-string-escaping))))
          ('in-string
           (pcase (aref str i)
             (?\\ (setq eltr--read-state 'in-string-escaping))
             (?\" (setq eltr--read-state 'out-of-string))))
          ('out-of-string-escaping
           (setq eltr--read-state 'out-of-string))
          ('in-string-escaping
           (setq eltr--read-state 'in-string)))
        (when (< eltr--open-parenses-count 0)
          (eltr-print-error "Unmatched \")\"\n")
          (throw 'unbalanced (substring str 0 (- i 1)))))
      str)))

(defun eltr-substitute-history (str)
  "Make history substitutions in STR.
,,eNN --> the input expression of index NN
,,vNN --> the returned value of index NN"
  (eltr-assert (stringp str))
  (save-match-data
    (let ((pos 0)
          (matches nil))
      (while (string-match ",,[ev][0-9]+" str pos)
        (push (match-string 0 str) matches)
        (setq pos (match-end 0)))
      (let ((new-str str))
        (dolist (regexp matches)
          (let ((num (string-to-number (substring regexp 3)))
                (request (substring regexp 2 3))) ;; e = expression, v = value
            (let ((history-item (eltr-history-ref num)))

              (let ((subst (cond ((string-equal request "e")
                                  (prin1-to-string (eltr-history-item-expression history-item)))
                                 ((and (not (eltr-history-item-failed history-item))
                                       (string-equal request "v"))
                                  (concat "'" (prin1-to-string (eltr-history-item-value history-item))))
                                 (t "NIL")))) ;; default if user requested value of a failed expression
                (eltr-assert (stringp regexp))
                (eltr-assert (stringp subst))
                (eltr-assert (stringp new-str))
                (setq new-str (replace-regexp-in-string regexp subst new-str))))))
        new-str))))

(defun eltr-int-to-bin-str (n)
  "Return a string with the binary representation of N.
The number N should be a nonnegtive integer."
  (eltr-assert (integerp n))
  (eltr-assert (>= n 0))
  (let ((str ""))
    (while (not (zerop n))
      (setq str (concat (if (= 1 (logand n 1)) "1" "0")
                        str))
      (setq n (lsh n -1)))
    (if (string= str "")
        "0"
      str)))


(defun eltr-abbreviate-number (numstr max)
  "Abbreviates a number.
Given NUMSTR, a string representation of a number, return its
abbreviated form, with at most MAX digits.  When the number is
not fully shown, a trailing ellipsis is added."
  (eltr-assert (stringp numstr))
  (eltr-assert (integerp max))
  (if (or (< max 0)
          (>= max (length numstr)))
      numstr
      (format "%s..." (substring numstr 0 max))))


(defun eltr-format-result (res)
  "Format the result of evaulation in string RES."
  ;; #d, #c, #x, #b, #o
  (if (integerp res)
      (let ((str (eltr-abbreviate-number (format "%d" res) ; %d always shown
                                         eltr-show-max-dec-digits)))
        
        (let ((str2 ""))
          (when (or (and (>= res 0)       ; no negative-valued char!
                         (< res #x10ffff) ; over that there's no char!
                         eltr-show-int-as-char)
                  eltr-show-int-as-hex    ; want hex?
                  (and (>= res 0) eltr-show-int-as-bin) ; want bin?
                  eltr-show-int-as-octal) ; want octal?
    
            ;; char
            (when (and (>= res 0)
                       (< res #x10ffff)
                       eltr-show-int-as-char)
              
              (if (aref printable-chars res)
                  (setq str2 (concat str2 " ?" (string res)))
                (setq str2 (concat str2 " ?\\" (single-key-description res)))))
            
            ;; hex is easy
            (when eltr-show-int-as-hex
              (setq str2
                    (concat str2
                            (format " #x%s"  (eltr-abbreviate-number
                                              (format "%x" res)
                                              eltr-show-max-octal-digits)))))
            
            ;; bin
            (when (and (>= res 0)
                       eltr-show-int-as-bin)
              (let ((binstr (eltr-int-to-bin-str res)))
                (setq str2
                      (concat str2
                              (format " #b%s"
                                      (eltr-abbreviate-number binstr
                                                              eltr-show-max-binary-digits))))))
            
            ;; octal, easy
            (when eltr-show-int-as-octal
              (setq str2
                    (concat str2
                            (format " #o%s"
                                    (eltr-abbreviate-number
                                     (format "%o" res)
                                     eltr-show-max-octal-digits))))))
            
          (if (string-empty-p str2) str (concat str "\n  (" str2 " )"))))

    ;; else for (integerp res)
    (format "%s" res)))
  
;;;
;;; INPUT/OUTPUT
;;;

(defun eltr-echo-input-part (str start end)
  "Echo the relevant part of the input in STR from START to END.
The user may enter more than one expression at a time.  This
function echoes only the next expression to be evaluated, which
is between start and end.  It will will ignore trailing blanks.
If eltr-echo-input is non-NIL, then eltr-echo-input-prefix is
printed before echoing."
  (eltr-assert (stringp str))
  (eltr-assert (integerp start))
  (eltr-assert (integerp end))
  (let ((offset (eltr-next-nonblank str start)))
    (let ((str-part (substring str offset end)))
      (when eltr-echo-input
        (let ((prefix
               (eltr-colorize-string (if (stringp eltr-echo-input-prefix)
                                         eltr-echo-input-prefix
                                       "[W]: eltr-echo-input-prefix is not a string\n")
                                     eltr-echo-prefix-emphasis
                                     eltr-echo-prefix-color))
              (echo (eltr-colorize-string str-part
                                          eltr-echo-emphasis
                                          eltr-echo-color)))
          (princ (format "%s %s\n" prefix echo)))))))

(defun eltr-get-prompt ()
  "Return the ELTR prompt string, colorized if eltr-colorize is non-nil."
  (let ((prompt-nocolor  (if (stringp eltr-prompt-prefix)
                             (eltr-substitute-placehloders eltr-prompt-prefix)
                           "[W]: eltr-prompt-prefix is not a string!\n> ")))
    (eltr-colorize-string prompt-nocolor
                          eltr-prompt-emphasis
                          eltr-prompt-color)))

(defun eltr-get-input ()
  "Read a string from standard input.
Before reading, a prompt is displayed.  The prompt is the value
of eltr-prompt, which should be a string.  Returns a cons,
(str . fail), where str is the string read, and fail is non-nil
when an error occurred."
  (let ((fail nil))
    (let ((prompt (eltr-get-prompt)))
      (let ((str (condition-case nil
                     (read-string prompt)
                   (error (progn (setq fail t)
                                 nil)))))
        
        (unless (or fail (string= str ""))
          (setq str (eltr-count-parenses str)))
        
        (cons str fail)))))

(defun eltr-pause-before-next-input ()
  "If running interactively, wait for the user to press a key.
Otherwise the result of last eval won't be seen."
  (if noninteractive
      (terpri)
    (progn (princ "        (press any key)")
           (read-char))))

(defun eltr-read-from-string (str start)
  "Read a Lisp form from STR, beginning at START.
If an error occurs while reading, an error message is printed;
If no error occurs, then this function will return the same cons
that `read-from-string' returned, (exp . end), where exp is the
expression that was read, and end is the position in the string
where the expression ends."
  (eltr-assert (stringp str))
  (eltr-assert (integerp start))
  (condition-case err
      ;; no error, return the cons from read-from-string:
      (read-from-string str start)
    (error (progn (eltr-print-error (error-message-string err))
                  (terpri)
                  ;; on error, return nil:
                  nil))))


;;;
;;; EVAL
;;;

(defun eltr-eval (expr)
  "Evaluate EXPR, calling eval inside a `condition-case'.
If an error happens, its string is sent back to the user.
Returns a cons, in which the CAR is the result of eval, and the
CDR is a flag telling wether there was an error or not."
  (let ((error-flag nil))
    (let ((res (condition-case err
                   (eval expr)
                 (error (progn (eltr-print-error (error-message-string err))
                               (setq error-flag t)
                               nil)))))
      (cons res error-flag))))


;;;
;;; REPL
;;;

(defun eltr ()
  "ELTR entry point: starts a simple REPL, suitable for terminals."
  (interactive)

  ;; the minibuffer doesn't seem to understand ANSI color escape codes:
  (unless noninteractive
    (setq eltr-colorize nil))
  
  ;; maybe say hello?
  (when eltr-show-banner
    (eltr-hello))

  (let ((eltr-run t))
    (while eltr-run

      (let ((str-err (eltr-get-input)) ) ; -> (str . error)
        (cond ((cdr str-err)
               (princ "[no input] Goodbye!\n")
               (setq eltr-run nil))
              (t
               (let ((str (car str-err))
                     (start 0))

                 (let ((eltr-prompt-prefix ""))
                   (while (or (eq eltr--read-state 'in-string)
                              (> eltr--open-parenses-count 0))
                     ;; if the user sent a multiline string, add newlines
                     ;; after each line:
                     (when (eq eltr--read-state 'in-string)
                       (setq str (concat str "\n")))
                     ;; get more input:
                     (setq str-err (eltr-get-input)) ; will update the open parenses count
                     
                     (setq str (concat str (car str-err)))))

                 ;; do history substitutions:
                 ;; ,,eNN -> expression of index NN
                 ;; ,,vNN -> returned value of index NN
                 (let ((str-sub-hist (eltr-substitute-history str)))
                   (setq str str-sub-hist))

                 (when (eltr-next-nonblank str 0)
                   
                   ;; parse the string
                   (let ((expr-data (eltr-read-from-string str start)))
                     ;; - expr-data will be null on read errors, so if it its
                     ;; we stop reading;
                     ;; - eltr-run is our "keep running" flag
                     (while (and expr-data
                                 eltr-run)
                       (let ((expr (car expr-data))
                             (end  (cdr expr-data)))
                         (let ((offset (eltr-next-nonblank str start)))
                           (eltr-echo-input-part str offset end)
                           
                           ;; does user want to quit?
                           (if (string= ",q" (substring str offset end))
                               (progn (eltr-cleanup)
                                      (princ "[,q] Goodbye!\n")
                                      (setq eltr-run nil))
                             
                             ;; eval, get result
                             (let ((eval-result (eltr-eval expr)))
                               
                               (let ((res (car eval-result))
                                     (error-flag (cdr eval-result)))
                                 
                                 ;; only if there was no error we push expr and res into the
                                 ;; history stack
                                 (if error-flag
                                     (eltr-push-history expr t)
                                   
                                   ;; push history BEFORE substituting placeholders, because those
                                   ;; will need the current history item
                                   (eltr-push-history expr nil res)
                                   (let ((prefix (eltr-substitute-placehloders eltr-result-prefix))
                                         (suffix (eltr-substitute-placehloders eltr-result-suffix)))
                                     (princ
                                      (concat (eltr-colorize-string prefix
                                                                    eltr-result-prefix-emphasis
                                                                    eltr-result-prefix-color)
                                              (eltr-colorize-string (eltr-format-result res)
                                                                    eltr-result-emphasis
                                                                    eltr-result-color)
                                              (eltr-colorize-string suffix
                                                                    eltr-result-suffix-emphasis
                                                                    eltr-result-suffix-color))))))
                               (setq eltr--next-expr (+ eltr--next-expr 1)))
                             
                             (eltr-pause-before-next-input)
                             
                             (setq start end)

                             ;; if there's no more input, set expr-data to nil, so
                             ;; we'll exit the loop
                             (if (eltr-next-nonblank str start)
                                 (setq expr-data (eltr-read-from-string str start))
                               (setq expr-data nil)))))))))))))))

(provide 'eltr)

;;; eltr.el ends here
