\input texinfo    @c -*- texinfo -*-
@c SPDX-License-Identifier: GFDL-1.3-no-invariants-or-later
@c %**start of header
@setfilename eltr.info
@settitle ELTR 0.0.4
@documentencoding UTF-8
@documentlanguage en
@syncodeindex pg cp
@c %**end of header

@copying
This manual is for ELTR (version 0.0.4, last updated 4 Oct 2023).

Copyright @copyright{} 2020 Jerônimo Pellegrini

@quotation
Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with no Invariant Sections, with no Front-Cover Texts,
and with no Back-Cover Texts.  A copy of the license is included in
the section entitled ``GNU Free Documentation License''.
@end quotation
@end copying

@dircategory Emacs
@direntry
* ELTR: (eltr)                  Emacs Lisp Terminal REPL.
@end direntry

@finalout
@titlepage
@title ELTR
@subtitle for version 0.0.4, last updated 4 Oct 2023
@author Jerônimo Pellegrini (@email{j_p@@aleph0.info})
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@contents

@ifnottex
@node Top
@top ELTR

This manual is for ELTR (version 0.0.4, last updated 4 October 2023).
@end ifnottex

@menu
* What is ELTR?::
* Invoking ELTR::
* Customizing ELTR::
* Issues::
* GNU Free Documentation License::
* Index::
@end menu

@node What is ELTR?
@chapter What is ELTR?

ELTR is ``Emacs Lisp Terminal Repl'' -- a simple REPL for Emacs Lisp which runs on the command line.
It behaves as REPLs of other languages. The following example illustrates what ELTR does.

@example
@verbatim
$ eltr
Emacs version 28.0.50 -- fa65c044f2ebe666467166075c1507a8d0e1347f,
built 2021-04-24 17:19:42 on socrates.
It will be a nice day. I can feel it!
1> (sqrt 2) 
;; (sqrt 2)
1=> 1.4142135623730951
                       ( float )
2> (concat "two " "strings")
;; (concat "two " "strings")
2=> two strings
                       ( string )
3> emacs-version
;; emacs-version
3=> 28.0.50
                       ( string )
@end verbatim
@end example

ELTR's default prompt (it can be customized) is @code{n>}, where n is
the input number. After each input, ELTR

@enumerate
@item echoes the input after @code{;;}
@item shows the result, after a prefix (@code{n=>} by default);
@item shoes the type of the output between paretheses.
@end enumerate

If the returned value is an integer, then ELTR will show its value
in base 10, and also as character, hexadecimal, binary, and octal.

@example
@verbatim
1> 42
;; 42
1=> 42
  ( ?* #x2a #b101010 #o52 )
                       ( integer )
@end verbatim
@end example

When an integer corresponds to a non-printable character, its
character representation as keyboard sequence is shown.
If the integer does not have a corresponding character (it's
outside the Unicode range), then it is not shown.

@example
@verbatim
2> 10
;; 10
2=> 10
  ( ?\C-j #xa #b1010 #o12 )
                       ( integer )

3> #xffffffffffff
;; #xffffffffffff
3=> 281474976710655
  ( #xffffffffffff #b111111111111111111111111111111... #o7777777777777777 )
                       ( integer )
@end verbatim
@end example

By default ELTR abbreviates integers printed in hex/bin/octal, so they fit
in the line. This can be changed.

It is possible, of course, to run @code{emacs -nw -f ielm}, but
then the whole terminal screen is taken by Emacs; ELTR behaves
mode closely to what other REPLs do.

Emacs functions that do not deal with frames and windows can be used
within ELTR; the buffers and the minibuffer can be handled, although
they're not used in user interaction as they are when you run Emacs
interactively.

Now, interestingly, it is also possible to run ELTR
@emph{while running Emacs interactively} -- the interaction happens in
the minibuffer.

@node Invoking ELTR
@chapter Invoking ELTR

@pindex eltr
@cindex invoking @command{eltr}

To invoke ELTR from the command line, just call

@example
@code{eltr}
@end example

If you want to pass options to the Emacs process that will be running the REPL, 
you can do that directly, as eltr will pass options along to Emacs:

@example
@code {eltr -l my-library.el}
@end example

You can also use ELTR from within Emacs. First,

@example
@code{(require 'eltr)}
@end example

then

@example
@kbd{M-x} @code{eltr}
@end example

You can finish ELTR interaction by entering @code{,q} as input.

@node Readline and history
@section Readline and history

I recommend using  rlwrap (@url{https://github.com/hanslub42/rlwrap}). It is possible to
add readline-like capability to eltr, but using it with rlwrap works fine on the command
line.

You can refer to previous expressions and returned values using 
@code{,,eNN} for expression of number NN and @code{,,vNN} for the returned value
of number NN.


@node Customizing ELTR
@chapter Customizing ELTR

@cindex customizing @command{eltr}

ELTR is heavily customizable.
The list below shows the variables that can be set to change its behavior.

General settings:

@itemize @bullet
@item @code{eltr-show-banner}: if true, ELTR will greet the user with a banner
@item @code{eltr-banner}: banner printed when ELTR starts
@item @code{eltr-history-size}: the number of expressions that ELTR will remember, and that the user can refer to with @code{,,e} and @code{,,v}
@end itemize

The following labels can be used in the @code{eltr-banner} variable:

@itemize @bullet
@item @code{$(version)} -- the version of ELTR
@item @code{$(emacs-version)} -- the version of Emacs on which ELTR is running
@end itemize

The labels mark the place where the version number will be inserted.


Input and output formatting and colorizing:

@itemize @bullet
@item @code{eltr-prompt-prefix}: prompt to be displayed when asking for user input
@item @code{eltr-result-prefix}: prefix to be displayed before showing evaluation result
@item @code{eltr-echo-input}: flag that tells wether the input should be echoed before eval
@item @code{eltr-echo-input-prefix}: default prefix to input echo
@item @code{eltr-colorize}: wether ELTR will colorize parts of the output. This is not syntax highlighting; it is only used to colorize the prompt and some other REPL decorations. When running inside Emacs, this is set to nil at startup because @code{princ} on the minibuffer doesn't support ANSI color escape codes
@item @code{eltr-prompt-color}: color used for the REPL prompt
@item @code{eltr-prompt-emphasis}: emphasis (bold, italic, underline, normal) used for the REPL prompt
@item @code{eltr-result-prefix-color}: color used for the REPL prompt
@item @code{eltr-result-prefix-emphasis}: emphasis (bold, italic, underline, normal) used for the REPL prompt
@item @code{eltr-result-color}: color used for the result output
@item @code{eltr-result-emphasis}: emphasis (bold, italic, underline, normal) used for the result output
@item @code{eltr-result-suffix-color}: color used for the result suffix
@item @code{eltr-result-suffix-emphasis}: emphasis (bold, italic, underline, normal) used for the result suffix
@item @code{eltr-error-color}: color used for error messages
@item @code{eltr-error-emphasis}: emphasis (bold, italic, underline, normal) used for error messages
@item @code{eltr-echo-prefix-color}: color used for the prefix to input echo
@item @code{eltr-echo-prefix-emphasis}: emphasis (bold, italic, underline, normal) used for the prefix to input echo
@item @code{eltr-echo-color}: color used for the input echo
@item @code{eltr-echo-emphasis}: emphasis (bold, italic, underline, normal) used for the input echo
@end itemize

The following labels can be used in the prefix and suffix variables:

@itemize @bullet
@item @code{$(nexpr)} -- sequential index of inputs and results
@item @code{$(tvalue)} -- the type of the returned result
@end itemize


The default prefixes and suffix are listed below.

@multitable @columnfractions .4 .6
@item @code{eltr-prompt-prefix}      @tab @code{"$(nexpr)> "}
@item @code{eltr-echo-input-prefix}  @tab @code{";;"}
@item @code{eltr-result-prefix}      @tab @code{"$(nexpr)=> "}
@item @code{eltr-result-suffix}      @tab @code{"\n                       ( $(tvalue) )"}
@end multitable


Printing of integers:

@itemize @bullet
@item @code{eltr-show-int-as-char}: when the REPL presents an integer result, should it also be shown as char?
@item @code{eltr-show-int-as-hex}: when the REPL presents an integer result, should it also be shown as hex?
@item @code{eltr-show-int-as-bin}: when the REPL presents an integer result, should it also be shown as binary?
@item @code{eltr-show-int-as-octal}: when the REPL presents an integer result, should it also be shown as octal?
@item @code{eltr-show-max-binary-digits}: maximum quantity of digits used in binary representations of integers on output
@item @code{eltr-show-max-octal-digits}: maximum quantity of digits used in octal representations of integers on output
@item @code{eltr-show-max-dec-digits}: maximum quantity of digits used in decimal representations of integers on output
@item @code{eltr-show-max-hex-digits}: maximum quantity of digits used in hex representations of integers on output
@end itemize

To let any number digits be printed, set the @code{eltr-show-max-*-digits} variable to -1.


The sequential number of expressions (read and evaluated) is kept in the internal variable @code{eltr--next-expr},
which can be changed to any number -- even non-integers or negative numbers.

@example
@verbatim
1> (setq eltr--next-expr -11.234)
;; (setq eltr--next-expr -11.234)
-11.234=> -11.234
                       ( float )
-10.234> 'a
;; 'a
-10.234=> a
                       ( symbol )
-9.234> 
@end verbatim
@end example

Within Emacs, you can also set the configuration variables using Emacs' usual configuration
interface (@kbd{M-x} @code{customize-group eltr}).

@node Issues
@chapter Issues

@cindex issues

ELTR does not work with Emacs' debuggers, and the @code{(debug)} function will quit the
program. It is possible to call @code{(backtrace)}, but it will likely show more of
ELTR's inernal functions invocation than the stack trace you wanted for your code.

@node GNU Free Documentation License
@appendix GNU Free Documentation License

@include fdl.texi

@node Index
@chapter Index

@printindex cp

@bye
