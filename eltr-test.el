;; SPDX-License-Identifier: AGPL-3.0-or-later

;; These are very rudimentary tests.

(setq load-path (cons "." load-path))
(load "eltr.el")

;;;
;;; The "load" above should either have loaded ELTR or
;;; failed! And the symbol 'eltr should be in the features
;;; list.
;;;

(ert-deftest eltr--loaded ()
  (should (member 'eltr features)))

;;;
;;; ELTR core
;;;

(ert-deftest eltr--eval ()
  ;; TODO: test more!
  (should (equal (eltr-eval '(+ 2 3)) '(5)))
  (should (equal (eltr-eval ''a-symbol) '(a-symbol)))
  (should (equal (eltr-eval '(/ 1 0)) '(nil . t)))
  (should (equal (eltr-eval (gensym "fresh-symbol")) '(nil . t))))

;;;
;;; auxiliary functions and macros
;;;

(ert-deftest eltr--assert ()
  (should-error (eltr-assert (error "Kaboom")))
  (should-error (eltr-assert (/ 1 0)))
  (should (= 2 (progn (eltr-assert t)
                      (+ 1 1)))))

(ert-deftest eltr--next-nonblank ()
  ;; TODO: negative number as second arg
  (should (eq (eltr-next-nonblank "   abc def   ghi" 1) 3))
  (should (eq (eltr-next-nonblank "   abc def   ghi" 4) 4))
  (should (eq (eltr-next-nonblank "   abc def   ghi" 6) 7))
  (should (eq (eltr-next-nonblank "   abc def   ghi" 10) 13))
  (should (eq (eltr-next-nonblank "   abc def   ghi" 14) 14))
  (should (eq (eltr-next-nonblank "   abc def   ghi" 16) nil))
  (should-error  (eq (eltr-next-nonblank "   abc def   ghi" 30)))
  (should-error  (eq (eltr-next-nonblank 'a-symbol 1))))

(ert-deftest eltr--int-to-bin-str ()
  (should (string= (eltr-int-to-bin-str 0) "0"))
  (should (string= (eltr-int-to-bin-str 15) "1111")))

;;;
;;; history-related tests
;;;

;; TODO: test ,,v and ,,e

(ert-deftest eltr--cleanup ()
  (let ((item '((* 2 3) nil 6)))
    (apply #'eltr-push-history item)
    (setq eltr--next-expr (+ eltr--next-expr 1)))
  (should (> eltr--next-expr 1))
  (should (not (null eltr--history)))
  (eltr-cleanup)
  (should (= eltr--next-expr 1))
  (should (null eltr--history)))

(ert-deftest eltr--history ()
  (eltr-cleanup)
  (let ((item1 '((+ 3 4) nil 7))
        (item2 '((- 3 4) nil -1))
        (item3 '((/ 1 0) t)))
    (should (eq (eltr-history-ref 1) nil))

    (apply #'eltr-push-history item1)
    (setq eltr--next-expr (+ eltr--next-expr 1))

    (apply #'eltr-push-history item2)
    (setq eltr--next-expr (+ eltr--next-expr 1))
    
    (apply #'eltr-push-history item3)
    (setq eltr--next-expr (+ eltr--next-expr 1))

    (should (= eltr--next-expr 4))
    (should (= (length eltr--history) 3))

    (should (equal (eltr-history-ref 1) (cons 1 item1)))
    (should (equal (eltr-history-ref 2) (cons 2 item2)))
    (should (equal (eltr-history-ref 3) (cons 3 item3)))

    (should (equal (eltr-history-item-expression
                    (eltr-history-ref 1)) '(+ 3 4)))
    (should (= (eltr-history-item-value
                (eltr-history-ref 1)) 7))
    (should (eq (eltr-history-item-failed
                 (eltr-history-ref 1)) nil))

    (should (equal (eltr-history-item-expression
                    (eltr-history-ref 2)) '(- 3 4)))
    (should (= (eltr-history-item-value
                (eltr-history-ref 2)) -1))
    (should (eq (eltr-history-item-failed
                 (eltr-history-ref 2)) nil))

    (should (equal (eltr-history-item-expression
                    (eltr-history-ref 3)) '(/ 1 0)))
    (should (eq (eltr-history-item-value
                 (eltr-history-ref 3)) nil))
    (should (eq (eltr-history-item-failed
                 (eltr-history-ref 3)) t))))

;;;
;;; colors
;;;

(ert-deftest eltr--colorize-string ()
  (let ((str "some uninteresting string")
        (error-str "error: wrong erroneous error"))
        
    (setq eltr-colorize nil)
    (let ((color-str (eltr-colorize-string str 'blue 'bold))
          (color-err (eltr-print-error error-str)))
      (should (eq (type-of color-str) 'string))
      (should (eq (type-of color-err) 'string))
      (should (string= str color-str))
      (should (string= color-err (concat "[E]: " error-str))))

    (setq eltr-colorize t)
    (let ((color-str  (eltr-colorize-string str 'blue 'bold))
          (color-err (eltr-print-error error-str)))
      (should (eq (type-of color-str) 'string))
      (should (eq (type-of color-err) 'string))
      (should (string= str
                       (substring color-str
                                  7
                                  (+ 7 (length str)))))
      (should (= (length color-str)
                 (+ 14 (length str))))
      (should (= (length color-err)
                 (+ 19 (length error-str)))))))



