# ELTR:  Emacs Lisp Terminal REPL
[![License](https://img.shields.io/badge/License-AGPL%20(%3E%3D3)-green)](https://spdx.org/licenses/AGPL-3.0-or-later.html)

ELTR is a simple REPL which runs on the command line. I have written as a toy project
because I wanted to see how far one could push Emacs into becoming similar to other 
languages with REPLs. I thought I'd just call IELM, but it won't run in batch mode. 
Then, a simple `(while t (princ (format "%s\n" (eval (read)))))` wouldn't work
really well either: it has a fixed prompt, which is a long string ("Lisp expression:"), and
would stop on errors. With that as a start, I ended up with this.

![ELTR running](screenshot.png "ELTR running")

There is a similar project on GihtHub: [emacsrepl](https://github.com/tetracat/emacsrepl)

This REPL, unlike IELM and [emacsrepl](https://github.com/tetracat/emacsrepl),
accepts more than one expression in each user input.


## Using ELTR

To start the REPL, type the following in the command line:

```
emacs --batch -l eltr.el -f eltr
```

Or, if you want to install it,

```
make install
```

Then just use `eltr` to start it. You probably want to use it with `rlwrap`.

ELTR passes its arguments to Emacs, so you can do

```
eltr -l my-file.el
```

and it will be equivalent to 

```
emacs -l my-file.el  --batch -l eltr.el -f eltr
```

so you can use functions and variables from whatever files you'd like. The `-Q`, `-q` and other arguments should 
also work; however, see that `-Q` may prevent Emacs from finding `eltr.el`.

You can use `,q` to quit the REPL.

There are several variables that can be set after ELTR is running, to change its behavior -- they are documented
in the manual.
Some of these are:

* `eltr-prompt-prefix`: prompt to be displayed when asking for user input
* `eltr-result-prefix`: prefix to be displayed before showing evaluation result
* `eltr-echo-input`: flag that tells wether the input should be echoed before eval
* `eltr-echo-input-prefix`: default prefix to input echo
* `eltr-history-size`: the number of expressions that ELTR will remember, and that the user can refer to with `,,e` and `,,v`

The following labels can be used in the prefix and suffix variables:

`$(nexpr)` -- sequential index of inputs and results
`$(tvalue)` -- the type of the returned result

The default prefixes and suffix are listed below.

| variable                | default value                             |
|:------------------------|:------------------------------------------|
|`eltr-prompt-prefix`     | `"$(nexpr)> "`                            |
|`eltr-echo-input-prefix` | `";;"`                                    |
|`eltr-result-prefix`     | `"$(nexpr)=> "`                           |
|`eltr-result-suffix`     | `"\n                       ( $(tvalue) )"`|


## Inside Emacs

### ELTR native

Although ELTR was designed to run on a terminal, you can run eltr in interactive mode inside a running
Emacs with `M-x` eltr. The interaction will be done in the minibuffer. You may need to use

```
(require 'eltr)
```

When running in interactive mode, the input and output will be done in the minibuffer.
In order to give you time to see the output, ELTR will wait for a key to be pressed.
In batch mode, it will just print the output and ask for the next expression.

Within Emacs, you can also set the configuration variables using Emacs' usual configuration
interface (`M-x` customize-group eltr).

### With eepitch / eev

ELTR works with [`eepitch`](http://anggtwu.net/eepitch.html) (which is part of [`eev`](http://anggtwu.net/index.html#eev) by Eduardo Ochs).

## Readline and history

I recommend using [rlwrap](https://github.com/hanslub42/rlwrap). It is possible to
add readline-like capability to eltr, but using it with rlwrap works fine on the command
line.

You can refer to previous expressions and returned values using 
`,,eNN` for expression of number NN and `,,vNN` for the returned value
of number NN.

## License

ELTR is distributed under the [GNU Affero GPL](https://www.gnu.org/licenses/agpl-3.0.html) 
license -- version 3 or, at your option, a later version.

The manual is released under the 
[GNU Free Documentation License](https://www.gnu.org/licenses/fdl-1.3.html) -- version 1.3
or at your option, a later version.
